;;;;
;;;; quaternion.lisp
;;;;

;;;; We orientate with the source file from Haskell's complex module.
;;;; On top we'll provide a readmacro #q to read in quaternions similar to #c.

(defpackage #:quaternion
  ;; Maybe some symbols have to be shadowed.
  (:use #:cl)
  (:shadow #:number
           #:realpart
           #:imagpart
           #:|SETF SLOT-VALUE|)
  (:export #:quaternion
           #:with-quaternion
           #:number
           #:imagpart
           #:realpart
           #:vector-part
           ))

(in-package #:quaternion)

(defgeneric (setf slot-value) (new-value object slot-name)
  (:method (new-value object (slot-name symbol))
    (setf (cl:slot-value object slot-name) new-value))
  (:method ((new-value real) (object quaternion) (slot-name (eql '%r)))
    (setf (cl:slot-value object slot-name) new-value)))

(defclass quaternion ()
  ((%r :initarg :r
       :initform 0
       :type real)
   (%i :initarg :i
       :initform 0
       :type real)
   (%j :initarg :j
       :initform 0
       :type real)
   (%k :initarg :k
       :initform 0
       :type real))
  (:documentation "A quaternion number consists of four components."))

(defmacro with-quaternion ((&optional r i j k) name &body body)
  "Have a convenience macro for shortening WITH-SLOTS on quaternions."
  (let ((symbol-list (list r i j k)))
    (assert (every #'(lambda (s) (typep s '(or null (and symbol (not keyword))))) symbol-list))
    `(with-slots ,(loop for symbol in symbol-list
                        for slot in '(%r %i %j %k)
                        until (null symbol)
                        collect (list symbol slot))
         ,name
       ,@body)))

(defmethod initialize-instance :after ((q quaternion) &key)
  (with-slots (%r %i %j %k) q
    (when (every #'integerp (list %r %i %j %k))
      (change-class q 'integer-quaternion))))

(defmethod update-instance-for-different-class ((q1 quaternion) (q2 integer-quaternion) &key)
  (with-quaternion (r1 i1 j1 k1) q1
    (with-quaternion (r2 i2 j2 k2) q2
      (setq r2 r1
            i2 i1
            j2 j1
            k2 k1))))

(defclass integer-quaternion (quaternion) ())

(defun quaternion (&optional (r 0) (i 0) (j 0) (k 0))
  "Instead of naming it MAKE-QUATERNION."
  (make-instance 'quaternion :r r :i i :j j :k k))

(defmethod print-object ((q quaternion) stream)
  (with-quaternion (r i j k) q)
  (format stream "#q(~a ~a ~a ~a)" r i j k))

(deftype number ()
  "The quaternions are also numbers."
  '(or quaternion cl:number))

(defgeneric realpart (number)
  (:method ((n cl:number))
    (cl:realpart n))
  (:method ((q quaternion))
    (slot-value q '%r)))

(defgeneric imagpart (number)
  (:method ((n cl:number))
    (cl:imagpart n))
  (:method ((q quaternion))
    (with-slots (%i %j %k) q
      (values %i %j %k))))

(defgeneric vector-part (number)
  (:method ((q quaternion))
    (coerce (multiple-value-list (imagpart q)) 'simple-vector)))

(defmethod conjugate ((q quaternion))
  (with-quaternion (r i j k) q
    (declare (ignore r))
    (setq i (- i)
          j (- j)
          k (- k))))
